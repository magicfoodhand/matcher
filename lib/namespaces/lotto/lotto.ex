defmodule Matcher.Lotto do
  @moduledoc false
  def find_match(request) do
    case request do
      'PB' -> 'Power Ball'
      _ -> Matcher.unknown_request(:lotto, request)
    end
  end
end
