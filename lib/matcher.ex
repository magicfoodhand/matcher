defmodule Matcher do
  @moduledoc """
  Matcher keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def unknown_request(namespace, request),
      do: unknown_request(namespace, request, :unknown_request)

  def unknown_request(namespace, request, error_type), do:
    %{ namespace: namespace, request: request, error: error_type }

  alias Matcher.Lotto

  def find_match(namespace, request) do
    case namespace do
      :core ->
        case request do
          :meaning_of_life -> 42
          _ -> unknown_request(namespace, request)
        end
       :lotto -> Lotto.find_match(request)
       _ -> unknown_request(namespace, request, :unknown_namespace)
    end
  end
end
