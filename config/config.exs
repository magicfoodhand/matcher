# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :matcher,
  ecto_repos: [Matcher.Repo]

# Configures the endpoint
config :matcher, MatcherWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "mZBn1NZsl7AW+BK+pTx/t6wATRzN14ArHqj51qi/vbNNHSak5jHyPkyLG/Q7Vnse",
  render_errors: [view: MatcherWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Matcher.PubSub,
  live_view: [signing_salt: "keibOb0X"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
