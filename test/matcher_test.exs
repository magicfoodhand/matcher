defmodule Matcher.MatcherTest do
  use ExUnit.Case

  alias Matcher

  @moduletag :capture_log

  doctest Matcher

  def matches_expected(namespace, request, expected) do
    assert Matcher.find_match(namespace, request) == expected
  end

  test "check the meaning of life", do: matches_expected(:core, :meaning_of_life, 42)

  describe "matching errors" do
    test "invalid request `testing`" do
      expected = %{ namespace: :core, request: :testing, error: :unknown_request }
      matches_expected(:core, :testing, expected)
    end

    test "invalid namespace `testing`" do
      expected = %{ namespace: :testing, request: :meaning_of_life, error: :unknown_namespace }
      matches_expected(:testing, :meaning_of_life, expected)
    end
  end

  describe "matching lotto" do
    def matches_lotto(request, expected), do: matches_expected(:lotto, request, expected)

    test "powerball matches" do
      matches_lotto('PB', 'Power Ball')
    end

    test "invalid request `testing`" do
      expected = %{ namespace: :lotto, request: :testing, error: :unknown_request }
      matches_expected(:lotto, :testing, expected)
    end
  end
end
